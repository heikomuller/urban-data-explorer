/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { RECEIVE_API } from './Api'
import { BASE_URL } from '../app'
import { decorate_listing_url } from '../resources';


/**
 * Actions dispatched by the domain page container.
 */

export const DOMAIN_PAGE_ERROR = 'PAGE_ERROR'
export const DOMAIN_PAGE_CLEAR_SELECT_COLUMN = 'DOMAIN_PAGE_CLEAR_SELECT_COLUMN';
export const DOMAIN_PAGE_RECEIVE_DATABASE = 'DOMAIN_PAGE_RECEIVE_DATABASE';
export const DOMAIN_PAGE_RECEIVE_DOMAINS = 'DOMAIN_PAGE_RECEIVE_DOMAINS';
export const DOMAIN_PAGE_RECEIVE_DOMAIN_COLUMNS = 'DOMAIN_PAGE_RECEIVE_DOMAIN_COLUMNS';
export const DOMAIN_PAGE_RECEIVE_DOMAIN_TERMS = 'DOMAIN_PAGE_RECEIVE_DOMAIN_TERMS';
export const DOMAIN_PAGE_SELECT_COLUMN = 'DOMAIN_PAGE_SELECT_COLUMN';
export const DOMAIN_PAGE_SELECT_COLUMN_TERM = 'DOMAIN_PAGE_SELECT_COLUMN_TERM';
export const DOMAIN_PAGE_SELECT_DOMAIN = 'DOMAIN_PAGE_SELECT_DOMAIN';
export const DOMAIN_PAGE_SELECT_TERM = 'DOMAIN_PAGE_SELECT_TERM';


export const fetchColumnTerms = (column, url) => (dispatch) => {
    dispatch({type: DOMAIN_PAGE_SELECT_COLUMN, column, termListing: {terms: null, navigate: null}});
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json =>
                    dispatch({
                        type: DOMAIN_PAGE_SELECT_COLUMN,
                        column: column,
                        termListing: json
                    })
                );
            } else {
                // ERROR
                dispatch({
                    type: DOMAIN_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch column terms'
                });
            }
        })
        .catch(err => dispatch({
            type: DOMAIN_PAGE_ERROR,
            message: err.message
        })
    )
}


/**
 * Fetch the list of domains for a given database. Starts by fetching the
 * API service description. Then fetches the database domains.
 *
 * In case of an error the PAGE_ERROR action is dispatched. Only if the API
 * descriptor and the domains have been retrieved successfully the
 * RECEIVE_DATABASE_DOMAINS action is dispatched.
 */
export const fetchDatabaseDomains = (databaseId) => (dispatch) => {
    // Fetch the API descriptor first.
    return fetch(BASE_URL)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS: Get domain resource next
                response.json().then(api => {
                    dispatch({type: RECEIVE_API, api});
                    let url = api.links.find((ref) => (ref.rel === 'databases')).href;
                    url += '/' + databaseId;
                    fetch(url).then(function(response) {
                        if (response.status === 200) {
                            response.json().then(database => {
                                dispatch({
                                    type: DOMAIN_PAGE_RECEIVE_DATABASE,
                                    database: database
                                });
                                // Fetch domains
                                let url = database.links.find((ref) => (ref.rel === 'domains')).href;
                                dispatch(fetchDomains(decorate_listing_url(url)))
                            });
                        } else {
                            dispatch({
                                type: DOMAIN_PAGE_ERROR,
                                status: response.status,
                                message: 'Error fetching database'
                            });
                        }
                    });
                });
            } else {
                // ERROR
                response.json().then(json => dispatch({
                    type: DOMAIN_PAGE_ERROR,
                    status: response.status,
                    message: 'Error fetching API descriptor'
                }));
            }
        })
        .catch(err => dispatch({
            type: DOMAIN_PAGE_ERROR,
            message: err.message
        }))
}

/**
 * Fetch next page of domains in the domain listing.
 */
export const fetchDomains = (url) => (dispatch) => {
    dispatch({
        type: DOMAIN_PAGE_RECEIVE_DOMAINS,
        domainListing: {domains: null, naviage: null}
    })
    return fetch(url).then(function(response) {
        if (response.status === 200) {
            response.json().then(json => (
                    dispatch({
                        type: DOMAIN_PAGE_RECEIVE_DOMAINS,
                        domainListing: json
                    })
                )
            )
        } else {
            dispatch({
                type: DOMAIN_PAGE_ERROR,
                status: response.status,
                message: 'Unable to fetch domain'
            });
        }
    })
    .catch(err => dispatch({
        type: DOMAIN_PAGE_ERROR,
        status: 500,
        message: err.message
    }));
}


/**
 * Fetch list of columns a domain occurs.
 */
export const fetchDomainColumns = (url) => (dispatch) => {
    dispatch({
        type: DOMAIN_PAGE_RECEIVE_DOMAIN_COLUMNS,
        domainColumnListing: {columns: null, navigate: null}
    })
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json => (
                    dispatch({
                        type: DOMAIN_PAGE_RECEIVE_DOMAIN_COLUMNS,
                        domainColumnListing: json
                    })
                ));
                // Fetch domain columns
            } else {
                // ERROR
                dispatch({
                    type: DOMAIN_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch domain columns'
                });
            }
        })
        .catch(err => dispatch({
            type: DOMAIN_PAGE_ERROR,
            message: err.message
        }))
}


/**
 * Fetch list of terms in a domain.
 */
export const fetchDomainTerms = (url) => (dispatch) => {
    dispatch({
        type: DOMAIN_PAGE_RECEIVE_DOMAIN_TERMS,
        domainTermListing: {terms: null, navigate: null}
    })
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json => (
                    dispatch({
                        type: DOMAIN_PAGE_RECEIVE_DOMAIN_TERMS,
                        domainTermListing: json
                    })
                ));
            } else {
                // ERROR
                dispatch({
                    type: DOMAIN_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch domain terms'
                });
            }
        })
        .catch(err => dispatch({
            type: DOMAIN_PAGE_ERROR,
            message: err.message
        }))
}


export const selectColumn = (column) => (dispatch) => {
    if (column != null) {
        dispatch({
            type: DOMAIN_PAGE_SELECT_COLUMN,
            column,
            termListing: {terms: null, navigate: null}}
        );
        let url = column.links.find((ref) => (ref.rel === 'terms')).href;
        dispatch(fetchColumnTerms(column, decorate_listing_url(url)));
    } else {
        dispatch({type: DOMAIN_PAGE_CLEAR_SELECT_COLUMN})
    }
}


export const selectColumnTerm = (term) => (dispatch) => {
    dispatch({type: DOMAIN_PAGE_SELECT_COLUMN_TERM, term});
}

/**
 * Set the selected domain. The domain can be null to indicate that the
 * selection has been cleared.
 */
export const selectDomain = (database, domain) => (dispatch) => {
    dispatch({type: DOMAIN_PAGE_SELECT_DOMAIN, domain});
    if (domain != null) {
        let url = domain.links.find((ref) => (ref.rel === 'terms')).href;
        dispatch(fetchDomainTerms(decorate_listing_url(url)));
        url = domain.links.find((ref) => (ref.rel === 'columns')).href;
        dispatch(fetchDomainColumns(decorate_listing_url(url)));
    }
}


export const selectTerm = (term) => (dispatch) => {
    dispatch({type: DOMAIN_PAGE_SELECT_TERM, term});
}
