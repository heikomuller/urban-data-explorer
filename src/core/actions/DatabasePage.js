/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BASE_URL } from '../app'
import { decorate_listing_url } from '../resources';

export const DATABASE_PAGE_ERROR = 'DATABASE_PAGE_ERROR';
export const DATABASE_FETCH_COLUMN_DOMAINS = 'DATABASE_FETCH_COLUMN_DOMAINS';
export const DATABASE_FETCH_COLUMN_TERMS = 'DATABASE_FETCH_COLUMN_TERMS';
export const DATABASE_FETCH_DOMAIN_TERMS = 'DATABASE_FETCH_DOMAIN_TERMS';
export const DATABASE_RECEIVE_COLUMN_DOMAINS = 'DATABASE_RECEIVE_COLUMN_DOMAINS';
export const DATABASE_RECEIVE_COLUMN_TERMS = 'DATABASE_RECEIVE_COLUMN_TERMS';
export const DATABASE_RECEIVE_DOMAIN_TERMS = 'DATABASE_RECEIVE_DOMAIN_TERMS';
export const RECEIVE_DATABASE = 'RECEIVE_DATABASE';
export const RECEIVE_DATABASE_COLUMNS = 'RECEIVE_DATABASE_COLUMNS';
export const RECEIVE_DATABASE_DATASETS = 'RECEIVE_DATABASE_DATASETS';
export const DATABASE_SELECT_COLUMN = 'DATABASE_SELECT_COLUMN';
export const DATABASE_SELECT_DATASET = 'DATABASE_SELECT_DATASET'
export const DATABASE_SELECT_COLUMN_DOMAIN = 'DATABASE_SELECT_COLUMN_DOMAIN';
export const DATABASE_SELECT_COLUMN_TERM = 'DATABASE_SELECT_COLUMN_TERM';
export const DATABASE_SELECT_DOMAIN_TERM = 'DATABASE_SELECT_DOMAIN_TERM'

/**
 * Fetch list of columns.
 */
export const fetchColumns = (url) => (dispatch) => {
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json => (
                    dispatch({
                        type: RECEIVE_DATABASE_COLUMNS,
                        columnListing: json
                    })
                ));
            } else {
                // ERROR
                dispatch({
                    type: DATABASE_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch column listing'
                });
            }
        })
        .catch(err => dispatch({
            type: DATABASE_PAGE_ERROR,
            message: err.message
        }))
}


/**
 * Fetch single columns.
 */
export const fetchColumn = (database, url) => (dispatch) => {
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json => (dispatch(selectColumn(database, json))));
            } else {
                // ERROR
                dispatch({
                    type: DATABASE_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch column'
                });
            }
        })
        .catch(err => dispatch({
            type: DATABASE_PAGE_ERROR,
            message: err.message
        }))
}


/**
 * Fetch database from API. Starts by fetching the API service description.
 * Then uses the API's 'databases' reference to POST a request to retrieve the
 * database with the given identifier.
 *
 * In case of an error the DATABASE_PAGE_ERROR action is dispatched. Only if
 * the API descriptor and the database has been retrieved successfully the
 * RECEIVE_DATABASE action is dispatched.
 */
export const fetchDatabase = (databaseId, columnId) => (dispatch) => {
    // Fetch the API descriptor first.
    return fetch(BASE_URL)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS: Get domain resource next
                response.json().then(api => {
                    let url = api.links.find((ref) => (ref.rel === 'databases')).href;
                    url += '/' + databaseId;
                    fetch(url).then(function(response) {
                        if (response.status === 200) {
                            response.json().then(database => {
                                dispatch({
                                    type: RECEIVE_DATABASE,
                                    api: api,
                                    database: database
                                });
                                // Fetch datasets
                                let url = database.links.find((ref) => (ref.rel === 'datasets')).href;
                                url = decorate_listing_url(url);
                                dispatch(fetchDatasets(url));
                                // Fetch columns
                                url = database.links.find((ref) => (ref.rel === 'columns')).href;
                                url = decorate_listing_url(url);
                                dispatch(fetchColumns(url));
                                if (columnId != null) {
                                    dispatch(
                                        fetchColumn(
                                            database,
                                            url.substring(0, url.indexOf('?')) + '/' + columnId
                                        )
                                    );
                                }
                            });
                        } else {
                            dispatch({
                                type: DATABASE_PAGE_ERROR,
                                status: response.status,
                                message: 'Error fetching database'
                            });
                        }
                    });
                });
            } else {
                // ERROR
                dispatch({
                    type: DATABASE_PAGE_ERROR,
                    status: response.status,
                    message: 'Error fetching API descriptor'
                });
            }
        })
        .catch(err => dispatch({
            type: DATABASE_PAGE_ERROR,
            message: err.message
        }))
}


/**
 * Fetch list of datasets.
 */
export const fetchDatasets = (url) => (dispatch) => {
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json => (
                    dispatch({
                        type: RECEIVE_DATABASE_DATASETS,
                        datasetListing: json
                    })
                ));
            } else {
                // ERROR
                dispatch({
                    type: DATABASE_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch dataset listing'
                });
            }
        })
        .catch(err => dispatch({
            type: DATABASE_PAGE_ERROR,
            message: err.message
        }))
}


/**
 * Get domains for a selected column.
 */
export const fetchColumnDomains = (url) => (dispatch) => {
    dispatch({type: DATABASE_FETCH_COLUMN_DOMAINS})
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json =>
                    dispatch({
                        type: DATABASE_RECEIVE_COLUMN_DOMAINS,
                        domainListing: json

                    })
                );
            } else {
                // ERROR
                dispatch({
                    type: DATABASE_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch column domains'
                });
            }
        })
        .catch(err => dispatch({
            type: DATABASE_PAGE_ERROR,
            message: err.message
        })
    )
}


/**
 * Get terms for a selected domain.
 */
export const fetchDomainTerms = (url) => (dispatch) => {
    dispatch({type: DATABASE_FETCH_DOMAIN_TERMS})
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json =>
                    dispatch({
                        type: DATABASE_RECEIVE_DOMAIN_TERMS,
                        termListing: json

                    })
                );
            } else {
                // ERROR
                dispatch({
                    type: DATABASE_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch domain terms'
                });
            }
        })
        .catch(err => dispatch({
            type: DATABASE_PAGE_ERROR,
            message: err.message
        })
    )
}


/**
 * Get terms for a selected column.
 */
export const fetchColumnTerms = (url) => (dispatch) => {
    dispatch({type: DATABASE_FETCH_COLUMN_TERMS})
    return fetch(url)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json =>
                    dispatch({
                        type: DATABASE_RECEIVE_COLUMN_TERMS,
                        termListing: json

                    })
                );
            } else {
                // ERROR
                dispatch({
                    type: DATABASE_PAGE_ERROR,
                    status: response.status,
                    message: 'Unable to fetch column terms'
                });
            }
        })
        .catch(err => dispatch({
            type: DATABASE_PAGE_ERROR,
            message: err.message
        })
    )
}


/**
 * Set the selected column. The given column can be null indicating that the
 * selection is being cleared.
 */
export const selectColumn = (database, column) => (dispatch) => {
    // Independently whether the column is null or not we change the selection.
    dispatch({type: DATABASE_SELECT_COLUMN, column});
    let url;
    if (column != null) {
        url = column.links.find((ref) => (ref.rel === 'terms')).href;
        dispatch(fetchColumnTerms(decorate_listing_url(url)));
        url = column.links.find((ref) => (ref.rel === 'domains')).href;
        dispatch(fetchColumnDomains(decorate_listing_url(url)));
    }
}


/**
 * Set the selected dataset. The given dataset can be null indicating that the
 * selection is being cleared.
 */
export const selectDataset = (database, dataset) => (dispatch) => {
    // Independently whether the dataset is null or not we change the selection.
    dispatch({type: DATABASE_SELECT_DATASET, dataset});
    let url;
    if (dataset != null) {
        url = dataset.links.find((ref) => (ref.rel === 'columns')).href;
    } else {
        url = database.links.find((ref) => (ref.rel === 'columns')).href;
    }
    dispatch(fetchColumns(decorate_listing_url(url)));

}


/**
 * Set the selected term for a column in the database page. Fetch domain terms
 * if the selected domain is not null.
 */
export const selectColumnDomain = (domain) => (dispatch) => {
    dispatch({type: DATABASE_SELECT_COLUMN_DOMAIN, domain})
    if (domain != null) {
        let url = domain.links.find((ref) => (ref.rel === 'terms')).href;
        dispatch(fetchDomainTerms(decorate_listing_url(url)));
    }
};


/**
 * Set the selected term for a column in the database page.
 */
export const selectColumnTerm = (term) => (dispatch) => (
    dispatch({type: DATABASE_SELECT_COLUMN_TERM, term})
);


/**
 * Set the selected term for a domain in the database page.
 */
export const selectDomainTerm = (term) => (dispatch) => (
    dispatch({type: DATABASE_SELECT_DOMAIN_TERM, term})
);
