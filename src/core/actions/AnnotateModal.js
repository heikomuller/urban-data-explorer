/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { PUT_REQUEST } from './Api';


export const ANNOTATE_MODAL_ERROR = 'ANNOTATE_MODAL_ERROR'
export const CHANGE_ANNOTATION_VALUE = 'CHANGE_ANNOTATION_VALUE';
export const RECEIVE_RESOURCE_ANNOTATION = 'RECEIVE_RESOURCE_ANNOTATION'
export const SET_ANNOTATE_RESOURCE = 'SET_ANNOTATE_RESOURCE'


/**
 * Annotate the given resource with the given value. Sets the annotate
 * resource to null if no error occirs.
 */

export const annotateResource = (resource, value) => (dispatch) => {
    // This should show the spinner in the anntation modal (TODO: add busy
    // flag to resource state)
    dispatch({type: SET_ANNOTATE_RESOURCE, resource})
    const url = resource.links.find((ref) => (ref.rel === 'annotations')).href;
    let data = null;
    if (value >= 0) {
        data = {value}
    } else {
        data = {}
    }
    return fetch(url, PUT_REQUEST(data))
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS
                response.json().then(json =>
                    dispatch({type: SET_ANNOTATE_RESOURCE, resource: null})
                );
            } else {
                // ERROR
                dispatch({
                    type: ANNOTATE_MODAL_ERROR,
                    status: response.status,
                    message: 'Error updating annotation'
                });
            }
        })
        .catch(err => dispatch({
            type: ANNOTATE_MODAL_ERROR,
            message: err.message
        })
    )
}


/**
 * Change the annotation value for the current resource. Does not submit the
 * change to the server.
 */
export const changeAnnotationValue = (value) => (dispatch) => {
    dispatch({type: CHANGE_ANNOTATION_VALUE, value})
}


/**
 * Set the resource that is being annotated. If the resource is not null
 * fetch the current annotation value and annotation options for the resource.
 */
export const setAnnotateResource = (resource) => (dispatch) => {
    dispatch({type: SET_ANNOTATE_RESOURCE, resource})
    if (resource != null) {
        const url = resource.links.find((ref) => (ref.rel === 'annotations')).href;
        return fetch(url)
            .then(function(response) {
                if (response.status === 200) {
                    // SUCCESS
                    response.json().then(json =>
                        dispatch({
                            type: RECEIVE_RESOURCE_ANNOTATION,
                            annotation: json

                        })
                    );
                } else {
                    // ERROR
                    dispatch({
                        type: ANNOTATE_MODAL_ERROR,
                        status: response.status,
                        message: 'Unable to fetch annotations'
                    });
                }
            })
            .catch(err => dispatch({
                type: ANNOTATE_MODAL_ERROR,
                message: err.message
            })
        )
    }
}
