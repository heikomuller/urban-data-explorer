/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BASE_URL } from '../app'


export const MAIN_PAGE_ERROR = 'MAIN_PAGE_ERROR';
export const RECEIVE_REPOSITORY = 'RECEIVE_REPOSITORY';


/**
 * Fetch the list of avaiable database from API. Starts by fetching the
 * API service description. Then uses the API's 'databases' reference to
 * retrieve the database listing.
 *
 * In case of an error the MAIN_PAGE_ERROR action is dispatched. Only if the API
 * descriptor and the databases have been retrieved successfully the
 * RECEIVE_REPOSITORY action is dispatched.
 */
export const fetchDatabases = () => (dispatch) => {
    // Fetch the API descriptor first.
    return fetch(BASE_URL)
        .then(function(response) {
            if (response.status === 200) {
                // SUCCESS: Get domain resource next
                response.json().then(api => {
                    const url = api.links.find((ref) => (ref.rel === 'databases')).href;
                    fetch(url).then(function(response) {
                        if (response.status === 200) {
                            response.json().then(json => (
                                dispatch({
                                    type: RECEIVE_REPOSITORY,
                                    api: api,
                                    repository: json
                                })
                            ));
                        } else {
                            dispatch({
                                type: MAIN_PAGE_ERROR,
                                status: response.status,
                                message: 'Error fetching database listing'
                            });
                        }
                    });
                });
            } else {
                // ERROR
                dispatch({
                    type: MAIN_PAGE_ERROR,
                    status: response.status,
                    message: 'Error fetching API descriptor'
                });
            }
        })
        .catch(err => dispatch({
            type: MAIN_PAGE_ERROR,
            message: err.message
        }))
}
