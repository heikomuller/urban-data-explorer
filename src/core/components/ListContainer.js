/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Input, Grid, List, Popup } from 'semantic-ui-react';
import ContentSpinner from './util/ContentSpinner';
import GridContainer from './GridContainer';
import Navigator from './navigate/Navigator';
import '../../css/GridContainer.css';


class ListContainer extends GridContainer {
    static propTypes = {
        resources: PropTypes.object,
        title: PropTypes.string.isRequired,
        onNavigate: PropTypes.func.isRequired
    };
    constructor(props) {
        super(props);
        const { resources } = props;
        let showSearch = false;
        let searchValue = '';
        if (resources != null) {
            const { navigator } = resources;
            if (navigator != null) {
                const { filter } = navigator;
                if (filter != null) {
                    showSearch = true;
                    searchValue = filter
                }
            }
        }
        this.state = {showSearch, searchValue}
    }
    /**
     * Dismiss search form and search results.
     */
    handleDismissSearch = () => {
        this.setState({showSearch: false, searchValue: ''});
        const { resources, onNavigate } = this.props;
        onNavigate(resources.navigator.getDefaultUrl())
    }
    handleSearchChange = (event) => {
        this.setState({searchValue: event.target.value});
    }
    handleSearchKeyDown = (event) => {
        const { keyCode } = event;
        if (keyCode === 13) {
            const { searchValue } = this.state;
            const { resources, onNavigate } = this.props;
            onNavigate(resources.navigator.getSearchUrl(searchValue.trim()))
        }
    }
    /**
     * Show the search form.
     */
    handleShowSearch = () => {
        this.setState({showSearch: true, searchValue: ''});
    }
    /**
     * Show a navigable list of elements.
     */
    navigableList = (listitems, links, listSize, onNavigate) => {
        const { title } = this.props;
        const { showSearch, searchValue } = this.state;
        let searchForm = null;
        if (showSearch) {
            searchForm = (
                <div className='search-form'>
                    <Input
                        icon={
                            <Icon
                                name='close'
                                link
                                onClick={this.handleDismissSearch}
                            />}
                        autoFocus
                        fluid
                        size='small'
                        value={searchValue}
                        placeholder='Filter...'
                        onChange={this.handleSearchChange}
                        onKeyDown={this.handleSearchKeyDown}
                    />
                </div>
            );
        }
        return (
            <div className='grid-container'>
                <Grid columns={16}>
                    <Grid.Row>
                        <Grid.Column width={6}>
                            <div className='container-headline'>
                                <span className='container-headline'>
                                    {title}
                                </span>
                                <Popup
                                    trigger={
                                        <Icon
                                            name='search'
                                            link
                                            onClick={this.handleShowSearch}
                                        />
                                    }
                                    content='Filter by name'
                                    position='top center'
                                    size='small'
                                />
                            </div>
                        </Grid.Column>
                        <Grid.Column  width={10} textAlign='right'>
                            <Navigator
                                links={links}
                                size={listSize}
                                onClick={onNavigate}
                            />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                { searchForm }
                <List className='listing'>
                    {listitems}
                </List>
            </div>
        );
    }
    transition() {
        const { title } = this.props;
        return (
            <div className='grid-container'>
                <Grid columns={16}>
                    <Grid.Row>
                        <Grid.Column width={6}>
                            <div className='container-headline'>
                                <span className='container-headline'>
                                    {title}
                                </span>
                            </div>
                        </Grid.Column>
                        <Grid.Column  width={10} textAlign='right'>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <ContentSpinner size='small' text={'Fetching ' + title} />
            </div>
        );
    }
}

export default ListContainer;
