/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../../../css/Headline.css'


/**
 * Display database name as clickable headline.
 */
export class DatabaseHeadline extends Component {
    static propTypes = {
        db: PropTypes.object.isRequired,
        onClick: PropTypes.func
    }
    handleClick = () => {
        const { onClick } = this.props;
        onClick();
    }
    render() {
        const { db, onClick } = this.props;
        let title = null;
        if (onClick != null) {
            title = (
                <a className='database-headline' onClick={this.handleClick}>
                    <div className='database-headline'>
                        <span className='database-title'>{db.title}</span>
                        <span className='database-name'>{db.name}</span>
                    </div>
                </a>
            );
        } else {
            return (
                <div className='database-headline'>
                    <span className='database-title'>{db.title}</span>
                    <span className='database-name'>{db.name}</span>
                </div>
            );
        }
        return (
            <div className='database-headline'>
                { title }
            </div>
        );
    }
}
