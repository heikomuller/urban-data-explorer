/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Component } from 'react'
import spinner from '../../../img/ajax-loader-bert.gif';
import '../../../css/Spinner.css';


class Loader extends Component {
    /**
     * Show the default loader.
     */
    render() {
        return (
            <div className='spinner'>
                <img src={spinner} className="exec-spinner" alt="loading" />
                <p className='spinner'>Loading ...</p>
            </div>
        );
    }
}

export default Loader;
