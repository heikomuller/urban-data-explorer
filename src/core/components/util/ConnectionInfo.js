/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import PropTypes from 'prop-types';

import '../../../css/Connection.css'


/**
 * Show Web Service API Connection info and documentation link.
 */
export class ConnectionInfo extends React.Component {
    static propTypes = {
        api: PropTypes.object
    }
    render() {
        const { api } = this.props;
        if (api != null) {
            const { name, links } = api;
            if (links != null) {
                const url = links.find((ref) => (ref.rel === 'self')).href;
                return (
                    <div className='connection-info'>
                        <pre className='connection-info'>
                            {'[ Connected to '}
                            <span className='connection-name'>
                                {name}
                            </span>
                            {' : '}
                            <a className="connection-link" href={url}>
                                {url}
                            </a>
                            {' ]'}
                        </pre>
                    </div>
                );
            }
        }
        return null;
    }
}

export default ConnectionInfo;
