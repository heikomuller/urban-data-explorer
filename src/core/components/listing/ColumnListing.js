/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Icon, List } from 'semantic-ui-react';
import ListContainer from '../ListContainer';
import '../../../css/Listing.css';


class ColumnListing extends ListContainer {
    static propTypes = {
        resources: PropTypes.object,
        selectedColumn: PropTypes.object,
        title: PropTypes.string.isRequired,
        onAnnotate: PropTypes.func,
        onNavigate: PropTypes.func.isRequired,
        onSelect: PropTypes.func.isRequired,
        onShow: PropTypes.func
    };
    render() {
        const {
            resources,
            selectedColumn,
            onAnnotate,
            onNavigate,
            onSelect,
            onShow
        } = this.props;
        if (resources != null) {
            if (resources.isBusy()) {
                return this.transition();
            }
            const columns = resources.elements;
            const navigator = resources.navigator;
            let selectedId = -1;
            if (selectedColumn != null) {
                selectedId = selectedColumn.id;
            }
            const items = [];
            for (let i = 0; i < columns.length; i++) {
                const col = columns[i];
                const label = col.name + ' [' + col.id + '] (' + col.termCount + ' terms)';
                // The list item depends on whether the column is the selected
                // column or not
                let item = null;
                if (col.id === selectedId) {
                    let annotateIcon = null;
                    if (onAnnotate != null) {
                        annotateIcon = (
                            <Icon
                                name='comment outline'
                                link
                                onClick={() => (onAnnotate(col))}
                            />
                        );
                    }
                    let showIcon = null;
                    if (onShow != null) {
                        showIcon = (
                            <Icon
                                name='eye'
                                link
                                onClick={() => (onShow(selectedId))}
                            />
                        );
                    }
                    item = (
                        <List.Description>
                            <span className='selected-list-item'>{label}</span>
                            <span className='left-space-sm'>
                                <Icon
                                    name='delete'
                                    link
                                    onClick={() => (onSelect(null))}
                                />
                                { annotateIcon }
                                { showIcon }
                            </span>
                        </List.Description>
                    );
                } else {
                    item = (
                        <List.Description
                            className={'linked-list-item column-name'}
                             as='a'
                            onClick={() => (onSelect(col))}
                        >
                            {label}
                        </List.Description>
                    );
                }
                items.push(
                    <List.Content key={i}>
                        {item}
                    </List.Content>
                );
            }
            return this.navigableList(items, navigator, columns.length, onNavigate);
        } else {
            return this.spinner();
        }
    }
}

export default ColumnListing;
