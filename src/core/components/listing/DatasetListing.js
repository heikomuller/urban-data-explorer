/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Icon, List } from 'semantic-ui-react';
import ListContainer from '../ListContainer';
import '../../../css/App.css';
import '../../../css/Listing.css';


class DatasetListing extends ListContainer {
    static propTypes = {
        resources: PropTypes.object,
        selectedDataset: PropTypes.object,
        title: PropTypes.string.isRequired,
        onNavigate: PropTypes.func.isRequired,
        onSelect: PropTypes.func.isRequired
    };
    render() {
        const {
            resources,
            selectedDataset,
            onNavigate,
            onSelect
        } = this.props;
        if (resources != null) {
            if (resources.isBusy()) {
                return this.transition();
            }
            const datasets = resources.elements;
            const navigator = resources.navigator;
            let selectedId = -1;
            if (selectedDataset != null) {
                selectedId = selectedDataset.id;
            }
            const items = [];
            for (let i = 0; i < datasets.length; i++) {
                const ds = datasets[i];
                // The shown items depends on whether the dataset is selected
                // or not
                let item = null;
                if (ds.id === selectedId) {
                    item = (
                        <List.Description>
                            <span className='selected-list-item'>
                                {ds.name + ' (' + ds.columnCount + ')'}
                            </span>
                            <span className='left-space-sm'>
                                <Icon
                                    name='delete'
                                    link
                                    onClick={() => (onSelect(null))}
                                />
                            </span>
                        </List.Description>
                    );
                } else {
                    item = (
                        <List.Description
                            className={'linked-list-item dataset-name'}
                             as='a'
                            onClick={() => (onSelect(ds))}
                        >
                            {ds.name + ' (' + ds.columnCount + ')'}
                        </List.Description>
                    );
                }
                items.push(
                    <List.Content key={i}>
                        {item}
                    </List.Content>
                );
            }
            return this.navigableList(items, navigator, datasets.length, onNavigate);
        } else {
            return this.spinner();
        }
    }
}

export default DatasetListing;
