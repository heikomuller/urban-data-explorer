/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'semantic-ui-react';
import '../../../css/Navigator.css';


class DomainNavigator extends Component {
    static propTypes = {
        domain: PropTypes.object.isRequired,
        onClick: PropTypes.func.isRequired

    };
    render() {
        const { domain, onClick } = this.props;
        // Pre-intialize all links as inactive
        let navPrev = (<Icon className='inactive-link' disabled name='caret left' />);
        let navNext = (<Icon className='inactive-link' disabled name='caret right' />);
        // Test which links are present in the given navigation info object
        if (domain.prev >= 0) {
            navPrev =  (
                <Icon
                    link
                    className='active-link'
                    name='caret left'
                    onClick={() => onClick(domain.prev)}
                />
            );
        }
        if (domain.next >= 0) {
            navNext = (
                <Icon
                    link
                    className='active-link'
                    name='caret right'
                    onClick={() => onClick(domain.next)}
                />
            );
        }
        return (
            <div className='domain-navigator'>
                <span className='domain-navigator'>
                    { navPrev }
                    {' Domains '}
                    { navNext }
                </span>
            </div>
        );
    }
}

export default DomainNavigator;
