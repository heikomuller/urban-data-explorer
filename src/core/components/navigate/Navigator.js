/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Icon, Popup } from 'semantic-ui-react';
import '../../../css/Navigator.css';


class Navigator extends Component {
    static propTypes = {
        links: PropTypes.object,
        size: PropTypes.number.isRequired,
        onClick: PropTypes.func.isRequired,

    };
    handleClick = (url) => {
        const { onClick } = this.props;
        onClick(url);
    }
    render() {
        const { links, size } = this.props;
        if (links == null) {
            return null;
        }
        // Pre-intialize all links as inactive
        let navFirst = (<Icon className='inactive-link' disabled name='fast backward' />);
        let navPrev = (<Icon className='inactive-link' disabled name='caret left' />);
        let navNext = (<Icon className='inactive-link' disabled name='caret right' />);
        let navLast = (<Icon className='inactive-link' disabled name='fast forward' />);
        // Test which links are present in the given navigation info object
        if (links.first != null) {
            navFirst =  (
                <Popup
                    trigger={<Icon
                        name='fast backward'
                        link
                        onClick={() => this.handleClick(links.first)}
                    />}
                    content='Move to first page'
                    position='top center'
                    size='small'
                />
            );
        }
        if (links.prev != null) {
            navPrev =  (
                <Popup
                    trigger={<Icon
                        name='caret left'
                        link
                        onClick={() => this.handleClick(links.prev)}
                    />}
                    content='Move to previous page'
                    position='top center'
                    size='small'
                />
            );
        }
        if (links.next != null) {
            navNext =  (
                <Popup
                    trigger={<Icon
                        name='caret right'
                        link
                        onClick={() => this.handleClick(links.next)}
                    />}
                    content='Move to next page'
                    position='top center'
                    size='small'
                />
            );
        }
        if (links.last != null) {
            navLast =  (
                <Popup
                    trigger={<Icon
                        name='fast forward'
                        link
                        onClick={() => this.handleClick(links.last)}
                    />}
                    content='Move to last page'
                    position='top center'
                    size='small'
                />
            );
        }
        let text = null;
        if (links.count > size) {
            if (links.offset != null) {
                text = (links.offset + 1) + '-' + (links.offset + size) + ' of ' + links.count;
            } else {
                text = size + ' of ' + links.count;
            }
        } else {
            text = size + ' of ' + size;
        }
        return (
            <span className='navigator'>
                { navFirst }
                { navPrev }
                <span className='nav-text'>{ text }</span>
                { navNext }
                { navLast }
            </span>
        );
    }
}

export default Navigator;
