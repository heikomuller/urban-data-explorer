/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// -----------------------------------------------------------------------------
//
// App Urls
//
// -----------------------------------------------------------------------------

export const BASE_PATH = '/urban-explorer'

/**
 * Url for database page.
 */
export const database_url = (databaseId) => {
    return BASE_PATH + '/databases/' + databaseId;
}

/**
 * Url for domain page.
 */
export const database_domains_url = (databaseId) => {
    return database_url(databaseId) + '/domains/';
}


// -----------------------------------------------------------------------------
//
// API Urls
//
// -----------------------------------------------------------------------------

export const BASE_URL = 'http://cds-swg1.cims.nyu.edu:8080/urban-data-api/v1';
//export const BASE_URL = 'http://localhost:8080/urban-data-api/v1';
