/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//------------------------------------------------------------------------------
//
// HELPER CLASSES
//
// -----------------------------------------------------------------------------

export class ErrorObject {
    constructor(status, message) {
        this.status = status;
        this.message = message;
    }
    /**
     * True if the status of the error is 404
     */
    isFileNotFoundError() {
        return this.status === 404;
    }
}


export class Navigator {
    constructor(links) {
        this.first = links.first;
        this.last = links.last;
        this.prev = links.prev;
        this.next = links.next;
        this.full = links.full;
        this.offset = links.offset;
        this.limit = links.limit;
        this.count = links.count;
    }
    getDefaultUrl() {
        let url = this.full + '?offset=0'
        if (this.limit != null) {
            url += '&limit=' + this.limit;
        } else {
            url += '&limit=25';
        }
        return url;
    }
    getSearchUrl(query) {
        let url = this.full + '?filter=' + encodeURIComponent(query) + '&offset=0'
        if (this.limit != null) {
            url += '&limit=' + this.limit;
        }
        return url;
    }
}


export class ResourceListing {
    constructor(elements, links) {
        this.elements = elements;
        if (links != null) {
            this.navigator = new Navigator(links);
        } else {
            this.navigator = null;
        }
    }
    isBusy() {
        return (this.elements == null);
    }
}


// -----------------------------------------------------------------------------
//
// HELPER METHODS
//
// -----------------------------------------------------------------------------

export const decorate_listing_url = (url) => (url + '?offset=0&limit=25');
