/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Grid } from 'semantic-ui-react';
import {
    fetchDatabaseDomains, fetchDomains, fetchDomainColumns, fetchDomainTerms,
    fetchColumnTerms, selectColumn, selectColumnTerm, selectDomain, selectTerm
} from '../../actions/DomainPage';
import { setAnnotateResource } from '../../actions/AnnotateModal';
import AnnotateModal from '../annotate/AnnotateModal';
import ConnectionInfo from '../../components/util/ConnectionInfo';
import { ErrorMessage, NotFoundMessage } from '../../components/util/ErrorMessage';
import { DatabaseHeadline } from '../../components/util/Headline';
import Loader from '../../components/util/Loader';
import ColumnListing from '../../components/listing/ColumnListing';
import DomainListing from '../../components/listing/DomainListing';
import TermListing from '../../components/listing/TermListing';
import { database_url } from '../../app'
import '../../../css/App.css';


class DomainPage extends Component {
    static propTypes = {
        api: PropTypes.object,
        annotateResource: PropTypes.object,
        database: PropTypes.object,
        domainListing: PropTypes.object,
        domainColumnListing: PropTypes.object,
        domainTermListing: PropTypes.object,
        error: PropTypes.object,
        selectedDomain: PropTypes.object,
        selectedColumn: PropTypes.object,
        selectedColumnTerm: PropTypes.object,
        selectedTerm: PropTypes.object
    };
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        dispatch(fetchDatabaseDomains(this.props.match.params.database_id));
    }
    /**
     * Dispatch action to set the annotation resource.
     */
    handleAnnotateClick = (resource) => {
        const { dispatch } = this.props;
        dispatch(setAnnotateResource(resource));
    }
    /**
     * Navigate to a different page in the column terms listing.
     */
    handleColumnTermsNavigate = (url) => {
        const { dispatch, selectedColumn } = this.props;
        dispatch(fetchColumnTerms(selectedColumn, url));
    }
    handleColumnTermsSelect = (term) => {
        const { dispatch } = this.props;
        dispatch(selectColumnTerm(term));
    }
    /**
     * Navigate to a different page in the domain listing.
     */
    handleDomainNavigate = (url) => {
        const { dispatch } = this.props;
        dispatch(fetchDomains(url));
    }
    /**
     * Navigate to a different page in the domain columns listing.
     */
    handleDomainColumnsNavigate = (url) => {
        const { dispatch } = this.props;
        dispatch(fetchDomainColumns(url));
    }
    /**
     * Select a column in the domains column listing.
     */
    handleDomainColumnsSelect = (column) => {
        const { dispatch } = this.props;
        dispatch(selectColumn(column));
    }
    /**
     * Navigate to a different page in the domain terms listing.
     */
    handleDomainTermsNavigate = (url) => {
        const { dispatch } = this.props;
        dispatch(fetchDomainTerms(url));
    }
    /**
     * Handle selection of item in the domain listing. The given domain can
     * be null to indicate that the list selection has been cleared.
     */
    handleDomainSelect = (domain) => {
        const { dispatch, database } = this.props;
        if (domain != null) {
            dispatch(selectDomain(database, domain));
        } else {
            dispatch(selectDomain(domain));
        }
    }
    /**
     * Show page for given database.
     */
    handleShowDatabasePage = (columnId) => {
        const { history, database} = this.props;
        let url = database_url(database.id);
        if (columnId != null) {
            url = url + '?column=' + columnId;
        }
        history.push(url);
    }
    /**
     * Handle selection of item in domain term listing.
     */
    handleTermSelect = (term) => {
        const { dispatch } = this.props;
        dispatch(selectTerm(term));
    }
    /**
     * Show the domain terms and list of columns that contain the domain.
     */
    render() {
        const {
            api,
            annotateResource,
            database,
            domainListing,
            domainColumnListing,
            domainTermListing,
            error,
            selectedDomain,
            selectedColumn,
            selectedColumnTerm,
            selectedTerm
        } = this.props;
        let content = null;
        if (error != null) {
            if (error.isFileNotFoundError()) {
                content = (<NotFoundMessage message={error.message} />);
            } else {
                content = (<ErrorMessage message={error.message} />);
            }
        } else if ((database == null) || (domainListing == null)) {
            content = (<Loader />);
        } else {
            let domainTerms = null;
            if (domainTermListing != null) {
                domainTerms = (
                    <TermListing
                        resources={domainTermListing}
                        selectedTerm={selectedTerm}
                        title='Terms'
                        onAnnotate={this.handleAnnotateClick}
                        onNavigate={this.handleDomainTermsNavigate}
                        onSelect={this.handleTermSelect}
                    />
                );
            }
            let domainColumns = null;
            if (domainColumnListing != null) {
                domainColumns = (
                    <ColumnListing
                        resources={domainColumnListing}
                        selectedColumn={selectedColumn}
                        title='Columns'
                        onAnnotate={this.handleAnnotateClick}
                        onNavigate={this.handleDomainColumnsNavigate}
                        onSelect={this.handleDomainColumnsSelect}
                        onShow={this.handleShowDatabasePage}
                    />
                );
            }
            let columnTerms = null;
            if (selectedColumn != null) {
                columnTerms = (
                    <TermListing
                        resources={selectedColumn.termListing}
                        selectedTerm={selectedColumnTerm}
                        title='Terms'
                        onAnnotate={this.handleAnnotateClick}
                        onNavigate={this.handleColumnTermsNavigate}
                        onSelect={this.handleColumnTermsSelect}
                    />
                );
            }
            let annotateModal = null;
            if (annotateResource != null) {
                annotateModal = (<AnnotateModal />);
            }
            content = (
                <div>
                    <DatabaseHeadline db={database} onClick={this.handleShowDatabasePage}/>
                    <div className='page-grid'>
                        <Grid columns={16}>
                            <Grid.Row>
                                <Grid.Column width={4}>
                                    <DomainListing
                                        resources={domainListing}
                                        selectedDomain={selectedDomain}
                                        title='Domains'
                                        onAnnotate={this.handleAnnotateClick}
                                        onNavigate={this.handleDomainNavigate}
                                        onSelect={this.handleDomainSelect}
                                    />
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    { domainTerms }
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    { domainColumns }
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    { columnTerms }
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                        { annotateModal }
                    </div>
                </div>
            );
        }
        return (
            <div className='page-content'>
                {content}
                <ConnectionInfo api={api} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        api: state.domainPage.api,
        annotateResource: state.annotateModal.resource,
        database: state.domainPage.database,
        domainListing: state.domainPage.domainListing,
        domainColumnListing: state.domainPage.domainColumnListing,
        domainTermListing: state.domainPage.domainTermListing,
        error: state.domainPage.error,
        selectedDomain: state.domainPage.selectedDomain,
        selectedColumn: state.domainPage.selectedColumn,
        selectedColumnTerm: state.domainPage.selectedColumnTerm,
        selectedTerm: state.domainPage.selectedTerm
    }
}

export default withRouter(connect(mapStateToProps)(DomainPage))
