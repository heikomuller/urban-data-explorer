/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Card, Image } from 'semantic-ui-react';
import { fetchDatabases } from '../../actions/MainPage';
import ConnectionInfo from '../../components/util/ConnectionInfo';
import { ErrorMessage, NotFoundMessage } from '../../components/util/ErrorMessage';
import Loader from '../../components/util/Loader';
import { database_url, database_domains_url } from '../../app';
import dbIcon from '../../../img/database.png';
import '../../../css/App.css';
import '../../../css/Headline.css';
import '../../../css/MainPage.css';


class MainPage extends Component {
    static propTypes = {
        api: PropTypes.object,
        error: PropTypes.object,
        repository: PropTypes.object
    };
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        dispatch(fetchDatabases());
    }
    /**
     * Click handler for buttons that shows the database page.
     */
    handleShowDatabaseClick = (databaseId) => {
        const { history } = this.props;
        history.push(database_url(databaseId));
    }
    /**
     * Click handler for buttons that shows the domains page.
     */
    handleShowDomainsClick = (databaseId) => {
        const { history } = this.props;
        history.push(database_domains_url(databaseId));
    }
    render() {
        const { api, error, repository } = this.props;
        let content = null;
        if (error != null) {
            if (error.isFileNotFoundError()) {
                content = (<NotFoundMessage message={error.message} />);
            } else {
                content = (<ErrorMessage message={error.message} />);
            }
        } else if (repository == null) {
            content = (<Loader />);
        } else {
            const cards = [];
            for (let i = 0; i < repository.databases.length; i++) {
                const db = repository.databases[i];
                let logo = null;
                if (db.logo != null) {
                    logo = db.logo;
                } else {
                    logo = dbIcon;
                }
                cards.push(
                    <Card key={db.id}>
                        <Card.Content>
                            <Image floated='right' size='mini' src={logo} />
                            <Card.Header>{db.title}</Card.Header>
                            <Card.Meta><span className='database-meta'>{db.name}</span></Card.Meta>
                            <Card.Description>
                                <div className='database-description'>{db.description}</div>
                                <div className='database-datasets'>{db.datasetCount}<strong> Datasets</strong></div>
                                <div className='database-columns'>{db.columnCount}<strong> Columns</strong></div>
                                <div className='database-domains'>{db.domainCount}<strong> Domains</strong></div>
                            </Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                            <div className='ui two buttons'>
                                <Button
                                    basic
                                    fluid
                                    color='blue'
                                    onClick={() => this.handleShowDatabaseClick(db.id)}
                                    content='Datasets'
                                />
                                <Button
                                    basic
                                    fluid
                                    color='red'
                                    onClick={() => this.handleShowDomainsClick(db.id)}
                                    content='Domains'
                                />
                            </div>
                        </Card.Content>
                    </Card>
                );
            }
            content = (
                <div className='main-page-container'>
                    <h1 className='main-header'>Databases</h1>
                    <Card.Group stackable>
                        {cards}
                    </Card.Group>
                </div>
            );
        }
        return (
            <div className='page-content'>
                {content}
                <ConnectionInfo api={api} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        api: state.mainPage.api,
        error: state.mainPage.error,
        repository: state.mainPage.repository
    }
}

export default connect(mapStateToProps)(MainPage)
