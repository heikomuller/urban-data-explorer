/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Grid, Icon, Label } from 'semantic-ui-react';
import {
    fetchColumns, fetchDatabase, fetchDatasets, fetchColumnDomains,
    fetchColumnTerms, fetchDomainTerms,
    selectColumn, selectDataset, selectColumnDomain, selectColumnTerm,
    selectDomainTerm
} from '../../actions/DatabasePage';
import { setAnnotateResource } from '../../actions/AnnotateModal';
import AnnotateModal from '../annotate/AnnotateModal';
import ColumnListing from '../../components/listing/ColumnListing';
import DatasetListing from '../../components/listing/DatasetListing';
import DomainListing from '../../components/listing/DomainListing';
import TermListing from '../../components/listing/TermListing';
import ConnectionInfo from '../../components/util/ConnectionInfo';
import { DatabaseHeadline } from '../../components/util/Headline';
import { ErrorMessage, NotFoundMessage } from '../../components/util/ErrorMessage';
import Loader from '../../components/util/Loader';
import { database_domains_url } from '../../app'
import '../../../css/App.css';


class DatabasePage extends Component {
    static propTypes = {
        api: PropTypes.object,
        annotateResource: PropTypes.object,
        columnListing: PropTypes.object,
        database: PropTypes.object,
        columnDomainListing: PropTypes.object,
        columnTermListing: PropTypes.object,
        datasetListing: PropTypes.object,
        domainTermListing: PropTypes.object,
        error: PropTypes.object,
        selectedColumn: PropTypes.object,
        selectedDataset: PropTypes.object,
        selectedColumnDomain: PropTypes.object,
        selectedColumnTerm: PropTypes.object,
        selectedDomainTerm: PropTypes.object
    }
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        let columnId = this.props.location.search;
        if (columnId != null) {
            if (columnId.startsWith('?column=')) {
                columnId = columnId.substring(8);
            } else {
                columnId = null;
            }
        }
        dispatch(fetchDatabase(this.props.match.params.database_id, columnId));
    }
    getColumnPanel = () => {
        const { columnListing, selectedColumn } = this.props;
        if (columnListing != null) {
            return (
                <ColumnListing
                    resources={columnListing}
                    selectedColumn={selectedColumn}
                    title='Columns'
                    onAnnotate={this.handleAnnotateClick}
                    onNavigate={this.handleColumnNavigate}
                    onSelect={this.handleColumnSelect}
                />
            );
        } else {
            return null;
        }
    }
    getColumnDomainPanel = () => {
        const { columnDomainListing, selectedColumnDomain } = this.props;
        if (columnDomainListing != null) {
            return (
                <DomainListing
                    resources={columnDomainListing}
                    selectedDomain={selectedColumnDomain}
                    title='Domains'
                    onAnnotate={this.handleAnnotateClick}
                    onNavigate={this.handleColumnDomainNavigate}
                    onSelect={this.handleColumnDomainSelect}
                />
            );
        } else {
            return null;
        }
    }
    getColumnTermPanel = () => {
        const { columnTermListing, selectedColumnTerm } = this.props;
        if (columnTermListing != null) {
            return (
                <TermListing
                    resources={columnTermListing}
                    selectedTerm={selectedColumnTerm}
                    title='Terms'
                    onAnnotate={this.handleAnnotateClick}
                    onNavigate={this.handleTermNavigate}
                    onSelect={this.handleColumnTermSelect}
                />
            );
        } else {
            return null;
        }
    }
    getDatasetPanel = () => {
        const { datasetListing } = this.props;
        if (datasetListing != null) {
            return (
                <DatasetListing
                    resources={datasetListing}
                    title={'Datasets'}
                    onNavigate={this.handleDatasetNavigate}
                    onSelect={this.handleDatasetSelect}
                />
            );
        } else {
            return null;
        }
    }
    getDomainTermPanel = () => {
        const { domainTermListing, selectedDomainTerm } = this.props;
        if (domainTermListing != null) {
            return (
                <TermListing
                    resources={domainTermListing}
                    selectedTerm={selectedDomainTerm}
                    title='Terms'
                    onAnnotate={this.handleAnnotateClick}
                    onNavigate={this.handleDomainTermNavigate}
                    onSelect={this.handleDomainTermSelect}
                />
            );
        } else {
            return null;
        }
    }
    /**
     * Dispatch action to set the annotation resource.
     */
    handleAnnotateClick = (resource) => {
        const { dispatch } = this.props;
        dispatch(setAnnotateResource(resource));
    }
    /**
     * Load next page in column listing. If there is a current selected column
     * it has to be cleared.
     */
    handleColumnNavigate = (url) => {
        const { dispatch, database, selectedColumn } = this.props;
        dispatch(fetchColumns(url));
        if (selectedColumn != null) {
            dispatch(selectColumn(database));
        }
    }
    /**
     * Handle selection of item in the column listing. The given columnId can
     * be null to indicate that the list selection has been cleared.
     */
    handleColumnSelect = (column) => {
        const { dispatch, database } = this.props;
        if (column != null) {
            dispatch(selectColumn(database, column));
        } else {
            dispatch(selectColumn(database));
        }
    }
    /**
    * Load next page in column's domain listing. If there is a current selected
     * domain term it has to be cleared.
     */
    handleColumnDomainNavigate = (url) => {
        const { dispatch, selectedColumnDomain } = this.props;
        dispatch(fetchColumnDomains(url));
        if (selectedColumnDomain != null) {
            dispatch(selectColumnDomain());
        }
    }
    /**
     * Handle selection of an item in the column domain listing.
     */
    handleColumnDomainSelect = (domain) => {
        const { dispatch } = this.props;
        dispatch(selectColumnDomain(domain));
    }
    /**
     * Handle selection of an item in the column term listing.
     */
    handleColumnTermSelect = (term) => {
        const { dispatch } = this.props;
        dispatch(selectColumnTerm(term));
    }
    /**
     * Load page in dataset listing. If there is a selected dataset this has to
     * be cleared.
     */
    handleDatasetNavigate = (url) => {
        const { dispatch, database, selectedDataset } = this.props;
        dispatch(fetchDatasets(url));
        if (selectedDataset != null) {
            dispatch(selectDataset(database));
        }
    }
    /**
     * Handle selection of item in the dataset listing. The given dataset can
     * be null to indicate that the list selection has been cleared.
     */
    handleDatasetSelect = (dataset) => {
        const { dispatch, database } = this.props;
        if (dataset != null) {
            dispatch(selectDataset(database, dataset));
        } else {
            dispatch(selectDataset(database));
        }
    }
    handleDomainTermNavigate = (url) => {
        const { dispatch, selectedDomainTerm } = this.props;
        dispatch(fetchDomainTerms(url));
        if (selectedDomainTerm != null) {
            dispatch(selectDomainTerm());
        }
    }
    handleDomainTermSelect = (term) => {
        const { dispatch } = this.props;
        dispatch(selectDomainTerm(term));
    }
    handleShowDomainsPage = () => {
        const { history, database} = this.props;
        history.push(database_domains_url(database.id))
    }
    /**
     * Load next page in the term listing. If a term is currently selected the
     * selection is cleared.
     */
    handleTermNavigate = (url) => {
        const { dispatch, selectedColumnTerm  } = this.props;
        dispatch(fetchColumnTerms(url));
        if (selectedColumnTerm != null) {
            dispatch(selectColumnTerm());
        }
    }
    render() {
        const {
            api,
            annotateResource,
            database,
            error,
            selectedColumn,
            selectedDataset,
        } = this.props;
        let content = null;
        if (error != null) {
            if (error.isFileNotFoundError()) {
                content = (<NotFoundMessage message={error.message} />);
            } else {
                content = (<ErrorMessage message={error.message} />);
            }
        } else if (database == null) {
            content = (<Loader />);
        } else {
            // Grid layout
            let gridLayout = null;
            // Two panel layout with dataset and column listing.
            if ((selectedDataset == null) && (selectedColumn == null)) {
                gridLayout = (
                    <Grid width={16}>
                        <Grid.Row>
                            <Grid.Column width={6}>
                                { this.getDatasetPanel() }
                            </Grid.Column>
                            <Grid.Column width={6}>
                                { this.getColumnPanel() }
                            </Grid.Column>
                            <Grid.Column width={4}>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                );
            } else {
                gridLayout = (
                    <Grid width={16}>
                        <Grid.Row>
                            <Grid.Column width={4}>
                                { this.getColumnPanel() }
                            </Grid.Column>
                            <Grid.Column width={4}>
                                { this.getColumnTermPanel() }
                            </Grid.Column>
                            <Grid.Column width={4}>
                                { this.getColumnDomainPanel() }
                            </Grid.Column>
                            <Grid.Column width={4}>
                                { this.getDomainTermPanel() }
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                );
            }
            // Annotation modal
            let annotateModal = null;
            if (annotateResource != null) {
                annotateModal = (<AnnotateModal />);
            }
            // Sub-headline
            let subheader = null;
            if ((selectedDataset != null) && (selectedColumn == null)) {
                subheader = (
                    <Label as='a' onClick={() => (this.handleDatasetSelect())}>
                        {selectedDataset.name}
                        <span className='left-space-sm'>
                            <Icon name='delete' />
                        </span>
                    </Label>
                );
            } else if ((selectedDataset != null) && (selectedColumn != null)) {
                subheader = (
                    <span>
                        <Label as='a' onClick={() => (this.handleDatasetSelect())}>
                            {selectedDataset.name}
                            <span className='left-space-sm'>
                                <Icon name='delete' />
                            </span>
                        </Label>
                        <Label as='a' onClick={() => (this.handleColumnSelect())}>
                            {selectedColumn.name}
                            <Icon name='delete' />
                        </Label>
                    </span>
                );
            } else if ((selectedDataset == null) && (selectedColumn != null)) {
                subheader = (
                    <span>
                        <Label as='a' onClick={() => (
                            this.handleDatasetSelect(selectedColumn.dataset)
                        )}>
                            {selectedColumn.dataset.name}
                            <span className='left-space-sm'>
                                <Icon name='eye' />
                            </span>
                        </Label>
                        <Label as='a' onClick={() => (this.handleColumnSelect())}>
                            {selectedColumn.name}
                            <Icon name='delete' />
                        </Label>
                    </span>
                );
            }
            content = (
                <div>
                    <DatabaseHeadline db={database} onClick={this.handleShowDomainsPage}/>
                    { subheader }
                    <div className='page-grid'>
                        { gridLayout }
                    </div>
                    { annotateModal }
                </div>
            );
        }
        return (
            <div className='page-content'>
                {content}
                <ConnectionInfo api={api} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        api: state.databasePage.api,
        annotateResource: state.annotateModal.resource,
        columnListing: state.databasePage.columnListing,
        columnDomainListing: state.databasePage.columnDomainListing,
        columnTermListing: state.databasePage.columnTermListing,
        database: state.databasePage.database,
        datasetListing: state.databasePage.datasetListing,
        domainTermListing: state.databasePage.domainTermListing,
        error: state.databasePage.error,
        selectedColumn: state.databasePage.selectedColumn,
        selectedDataset: state.databasePage.selectedDataset,
        selectedColumnDomain: state.databasePage.selectedColumnDomain,
        selectedColumnTerm: state.databasePage.selectedColumnTerm,
        selectedDomainTerm: state.databasePage.selectedDomainTerm
    }
}

export default withRouter(connect(mapStateToProps)(DatabasePage))
