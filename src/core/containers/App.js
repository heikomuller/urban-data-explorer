/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import logo from '../../img/logo_small.png';
import MainPage from './page/MainPage';
import DatabasePage from './page/DatabasePage';
import DomainPage from './page/DomainPage';
import { BASE_PATH } from '../app';
import '../../css/App.css'

class App extends Component {
    render() {
        return (
            <div className="app">
                <div className="app-header">
                    <img src={logo} className="app-logo" alt="logo" />
                  <span className="app-name">
                      <a className="home-link" href={BASE_PATH}>
                          urban curation
                      </a>
                      <span className="app-title">data explorer</span>
                  </span>
                </div>
                <MuiThemeProvider>
                    <Router>
                        <Switch>
                            <Route exact path={BASE_PATH} component={MainPage} />
                            <Route exact path={'/urban-explorer/databases/:database_id/'} component={DatabasePage} />
                            <Route exact path={'/urban-explorer/databases/:database_id/domains'} component={DomainPage} />
                        </Switch>
                    </Router>
                </MuiThemeProvider>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {}
}

export default connect(mapStateToProps)(App)
