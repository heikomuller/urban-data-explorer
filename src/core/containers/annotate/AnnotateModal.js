/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {Component} from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import { Button, Form, Icon, Modal, Radio } from 'semantic-ui-react';
import {
    annotateResource, changeAnnotationValue, setAnnotateResource
} from '../../actions/AnnotateModal';
import { ErrorMessage } from '../../components/util/ErrorMessage';
import '../../../css/ContentSpinner.css';


class AnnotateModal extends Component {
    static propTypes = {
        error: PropTypes.object,
        options: PropTypes.array,
        resource: PropTypes.object.isRequired,
        value: PropTypes.number.isRequired
    };
    handleChange = (e, { value }) => {
        const { dispatch } = this.props;
        dispatch(changeAnnotationValue(value));
    }
    handleClose = () => {
        const { dispatch } = this.props;
        dispatch(setAnnotateResource());
    }
    handleSubmit = () => {
        const { dispatch, resource, value } = this.props;
        dispatch(annotateResource(resource, value));
    }
    render() {
        const { error, options, value } = this.props;
        let content = null;
        if (error != null) {
            content = (<ErrorMessage message={error.message} />);
        } else if (options != null) {
            let radio = [];
            for (let i = 0; i < options.length; i++) {
                const opt = options[i];
                radio.push(
                    <Form.Field key={opt.value}>
                        <Radio
                            label={opt.text}
                            name='optGroup'
                            value={opt.value}
                            checked={opt.value === value}
                            onChange={this.handleChange}
                        />
                    </Form.Field>
                );
            }
            content = (
                <Form>
                    { radio }
                </Form>
            );
        } else {
            content = (
                <div className='spinner-padding-md'>
                    <Icon name='spinner' size='big' loading/>
                </div>
            );
        }
        return (
            <Modal
                open={true}
                closeOnEscape={true}
                closeOnDimmerClick={true}
                onClose={this.handleClose}
            >
                <Modal.Header>Annotate as ...</Modal.Header>
                <Modal.Content>
                    { content }
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={this.handleClose}>Cancel</Button>
                    <Button
                        onClick={this.handleChange}
                        negative
                        disabled={value < 0}
                        value={-1}
                        content='Clear'
                    />
                    <Button
                        onClick={this.handleSubmit}
                        positive
                        content='Submit'
                    />
                </Modal.Actions>
            </Modal>
        );
    }
}


const mapStateToProps = state => {
    return {
        error: state.annotateModal.error,
        options: state.annotateModal.options,
        resource: state.annotateModal.resource,
        value: state.annotateModal.value
    }
}

export default connect(mapStateToProps)(AnnotateModal)
