/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Reducer for resource annotation modal.
 */

import {
    ANNOTATE_MODAL_ERROR, CHANGE_ANNOTATION_VALUE,
    RECEIVE_RESOURCE_ANNOTATION, SET_ANNOTATE_RESOURCE
} from '../actions/AnnotateModal';
import { ErrorObject } from '../resources'


const INITIAL_STATE = {
    error: null,
    resource: null,
    options: null,
    value: null
}


export default (state=INITIAL_STATE, action) => {
    switch (action.type) {
        case ANNOTATE_MODAL_ERROR:
            return {
                ...state,
                error: new ErrorObject(action.status, action.message)
            };
        case CHANGE_ANNOTATION_VALUE:
            return {...state, value: action.value};
        case RECEIVE_RESOURCE_ANNOTATION:
            console.log(action.annotation.value);
            return {
                ...state,
                options: action.annotation.options,
                value: (action.annotation.value != null) ? action.annotation.value : -1
            };
        case SET_ANNOTATE_RESOURCE:
            return {
                ...state,
                error: null,
                resource: action.resource,
                options: null,
                value: -1
            };
        default:
            return state;
    }
}
