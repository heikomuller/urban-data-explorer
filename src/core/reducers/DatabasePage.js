/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Reducer for database page content.
 */

import {
    DATABASE_PAGE_ERROR,
    DATABASE_FETCH_COLUMN_DOMAINS, DATABASE_FETCH_COLUMN_TERMS,
    DATABASE_FETCH_DOMAIN_TERMS,
    DATABASE_RECEIVE_COLUMN_DOMAINS, DATABASE_RECEIVE_COLUMN_TERMS,
    DATABASE_RECEIVE_DOMAIN_TERMS,
    RECEIVE_DATABASE, RECEIVE_DATABASE_COLUMNS, RECEIVE_DATABASE_DATASETS,
    DATABASE_SELECT_COLUMN, DATABASE_SELECT_DATASET,
    DATABASE_SELECT_COLUMN_DOMAIN, DATABASE_SELECT_COLUMN_TERM,
    DATABASE_SELECT_DOMAIN_TERM
} from '../actions/DatabasePage';
import { ErrorObject, ResourceListing } from '../resources'


 const INITIAL_STATE = {
     api: null,
     columnListing: null,
     columnDomainListing: null,
     columnTermListing: null,
     database: null,
     datasetListing: null,
     domainTermListing: null,
     error: null,
     selectedColumn: null,
     selectedDataset: null,
     selectedColumnDomain: null,
     selectedColumnTerm: null,
     selectedDomainTerm: null
 }


export default (state=INITIAL_STATE, action) => {
    switch (action.type) {
        case DATABASE_PAGE_ERROR:
            return {
                ...state,
                columnListing: null,
                database: null,
                datasetListing: null,
                domainTermListing: null,
                error: new ErrorObject(action.status, action.message),
                selectedColumn: null,
                selectedDataset: null,
                selectedColumnDomain: null,
                selectedColumnTerm: null,
                selectedDomainTerm: null,
                columnDomainListing: null,
                columnTermListing: null
            };
        case DATABASE_FETCH_COLUMN_DOMAINS:
            return {
                ...state,
                columnDomainListing: new ResourceListing(),
                domainTermListing: null,
                selectedDomainTerm: null
            }
        case DATABASE_FETCH_COLUMN_TERMS:
            return {
                ...state,
                columnTermListing: new ResourceListing()
            }
        case DATABASE_FETCH_DOMAIN_TERMS:
            return {
                ...state,
                domainTermListing: new ResourceListing()
            }
        case DATABASE_RECEIVE_COLUMN_DOMAINS:
            return {
                ...state,
                columnDomainListing: new ResourceListing(
                    action.domainListing.domains,
                    action.domainListing.navigate
                ),
                domainTermListing: null,
                selectedDomainTerm: null
            };
        case DATABASE_RECEIVE_COLUMN_TERMS:
            return {
                ...state,
                columnTermListing: new ResourceListing(
                    action.termListing.terms,
                    action.termListing.navigate
                )
            };
        case DATABASE_RECEIVE_DOMAIN_TERMS:
            return {
                ...state,
                domainTermListing: new ResourceListing(
                    action.termListing.terms,
                    action.termListing.navigate
                )
            };
        case DATABASE_SELECT_COLUMN:
            return {
                ...state,
                columnDomainListing: null,
                columnTermListing: null,
                domainTermListing: null,
                selectedColumn: action.column,
                selectedColumnDomain: null,
                selectedColumnTerm: null,
                selectedDomainTerm: null
            };
        case DATABASE_SELECT_DATASET:
            return {
                ...state,
                columnListing: new ResourceListing(),
                columnDomainListing: null,
                columnTermListing: null,
                domainTermListing: null,
                selectedDataset: action.dataset,
                selectedColumn: null,
                selectedColumnDomain: null,
                selectedColumnTerm: null,
                selectedDomainTerm: null
            };
        case DATABASE_SELECT_COLUMN_TERM:
            return {
                ...state,
                selectedColumnTerm: action.term
            };
        case DATABASE_SELECT_COLUMN_DOMAIN:
            return {
                ...state,
                domainTermListing: null,
                selectedColumnDomain: action.domain,
                selectedDomainTerm: null
            };
        case DATABASE_SELECT_DOMAIN_TERM:
            return {
                ...state,
                selectedDomainTerm: action.term
            };
        case RECEIVE_DATABASE:
            return {
                ...state,
                api: action.api,
                columnListing: null,
                columnDomainListing: null,
                columnTermListing: null,
                database: action.database,
                datasetListing: null,
                domainTermListing: null,
                error: null,
                selectedColumn: null,
                selectedDataset: null,
                selectedColumnDomain: null,
                selectedColumnTerm: null,
                selectedDomainTerm: null
            };
        case RECEIVE_DATABASE_COLUMNS:
            return {
                ...state,
                columnListing: new ResourceListing(
                    action.columnListing.columns,
                    action.columnListing.navigate
                ),
                columnDomainListing: null,
                columnTermListing: null,
                domainTermListing: null,
                selectedColumnDomain: null,
                selectedColumnTerm: null,
                selectedDomainTerm: null
            };
        case RECEIVE_DATABASE_DATASETS:
            return {
                ...state,
                columnDomainListing: null,
                columnTermListing: null,
                datasetListing: new ResourceListing(
                    action.datasetListing.datasets,
                    action.datasetListing.navigate
                ),
                domainTermListing: null,
                selectedDataset: null,
                selectedColumn: null,
                selectedColumnDomain: null,
                selectedColumnTerm: null,
                selectedDomainTerm: null
            };
         default:
             return state;
     }
 }
