/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Reducer for home page content.
 */

import { MAIN_PAGE_ERROR, RECEIVE_REPOSITORY } from '../actions/MainPage';
import { ErrorObject } from '../resources'


const INITIAL_STATE = {
    api: null,
    error: null,
    repository: null
}


export default (state=INITIAL_STATE, action) => {
    switch (action.type) {
        case RECEIVE_REPOSITORY:
            return {
                ...state,
                api: action.api,
                error: null,
                repository: action.repository
            };
        case MAIN_PAGE_ERROR:
            return {
                ...state,
                error: new ErrorObject(action.status, action.message),
                repository: null
            };
        default:
            return state;
    }
}
