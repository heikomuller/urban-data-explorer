/**
 * Copyright (C) 2018 New York University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Reducer for domain page content.
 */

import { RECEIVE_API } from '../actions/Api';
import {
    DOMAIN_PAGE_ERROR, DOMAIN_PAGE_RECEIVE_DATABASE,
    DOMAIN_PAGE_RECEIVE_DOMAIN_COLUMNS, DOMAIN_PAGE_RECEIVE_DOMAIN_TERMS,
    DOMAIN_PAGE_RECEIVE_DOMAINS, DOMAIN_PAGE_CLEAR_SELECT_COLUMN,
    DOMAIN_PAGE_SELECT_COLUMN, DOMAIN_PAGE_SELECT_COLUMN_TERM,
    DOMAIN_PAGE_SELECT_DOMAIN, DOMAIN_PAGE_SELECT_TERM
} from '../actions/DomainPage';
import { ErrorObject, ResourceListing } from '../resources'


const INITIAL_STATE = {
    api: null,
    database: null,
    domainListing: null,
    domainColumnListing: null,
    domainTermListing: null,
    error: null,
    selectedDomain: null,
    selectedColumn: null,
    selectedColumnTerm: null,
    selectedTerm: null
}


export default (state=INITIAL_STATE, action) => {
    switch (action.type) {
        case RECEIVE_API:
            return {...state, api: action.api};
        case DOMAIN_PAGE_RECEIVE_DATABASE:
            return {...state, database: action.database}
        case DOMAIN_PAGE_RECEIVE_DOMAINS:
            return {
                ...state,
                domainListing: new ResourceListing(
                    action.domainListing.domains,
                    action.domainListing.navigate
                ),
                domainColumnListing: null,
                domainTermListing: null,
                selectedDomain: null,
                selectedColumn: null,
                selectedColumnTerm: null,
                selectedTerm: null
            };
        case DOMAIN_PAGE_SELECT_DOMAIN:
            return {
                ...state,
                domainTermListing: null,
                domainColumnListing: null,
                selectedDomain: action.domain,
                selectedColumn: null,
                selectedColumnTerm: null,
                selectedTerm: null
            };
        case DOMAIN_PAGE_CLEAR_SELECT_COLUMN:
            return {...state, selectedColumn: null, selectedColumnTerm: null};
        case DOMAIN_PAGE_SELECT_COLUMN:
            return {
                ...state,
                selectedColumn: {
                    ...action.column,
                    termListing: new ResourceListing(
                        action.termListing.terms,
                        action.termListing.navigate
                    )
                },
                selectedColumnTerm: null
            };
        case DOMAIN_PAGE_SELECT_COLUMN_TERM:
            return {...state, selectedColumnTerm: action.term};
        case DOMAIN_PAGE_SELECT_TERM:
            return {...state, selectedTerm: action.term};
        case DOMAIN_PAGE_RECEIVE_DOMAIN_COLUMNS:
            return {
                ...state,
                domainColumnListing: new ResourceListing(
                    action.domainColumnListing.columns,
                    action.domainColumnListing.navigate
                ),
                selectedColumn: null,
                selectedColumnTerm: null
            };
        case DOMAIN_PAGE_RECEIVE_DOMAIN_TERMS:
            return {
                ...state,
                domainTermListing: new ResourceListing(
                    action.domainTermListing.terms,
                    action.domainTermListing.navigate
                ),
                selectedColumnTerm: null,
                selectedTerm: null
            };
        case DOMAIN_PAGE_ERROR:
            return {
                ...state,
                domainListing: null,
                domainColumnListing: null,
                domainTermListing: null,
                error: new ErrorObject(action.status, action.message),
                selectedDomain: null,
                selectedColumn: null,
                selectedColumnTerm: null,
                selectedTerm: null
            };
        default:
            return state;
    }
}
